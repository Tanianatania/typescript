import Fighter from '../public/fighter';

export const toggleFigterArray = (fighter: Fighter, fighters: Fighter[], condition?: boolean) => {
  console.log('condition',condition)
  if (fighters.find(toggleFigter => toggleFigter._id === fighter._id)) {
    return fighters.filter(figterItem => {
      return figterItem._id !== fighter._id
    })
  } else {
    return condition ? [...fighters, fighter] : [...fighters];
  }
}
