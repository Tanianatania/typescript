import Fighter from "../public/fighter";

export const getDetailsForm=(id:number,name:string,health:number,attack:number,defense:number,fighter: Fighter):string=>
{
  var result = `id <br>
  <input type="text" id="fighterId" value=${id}><br>
  name<br>
  <input type="text" id="fighterName" value=${name}><br>
  health<br>
  <input type="text" id="fighterHealth" value=${health}><br>
  attack<br>
  <input type="text" id="fighterAttack" " value=${attack}><br>
  defense<br>
  <input type="text" id="fighterDefense" value=${defense}><br><br>
  <input type="button" id="fighterSubmit" onclick="GetFighterValue(${fighter})" value="Submit">`;
  return result;
}