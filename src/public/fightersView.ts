import View from './view';
import FighterView from './fighterView';
import Fighter from './fighter';
import { toggleFigterArray } from '../utils';
import { fighterService } from './services/fightersService';
import {getDetailsForm} from '../utils/form';

class FightersView extends View {
  element: HTMLElement = null;
  fightersDetailsMap = null;
  selectedFigters: Fighter[];

  constructor(fighters: Fighter[]) {
    super();

    this.selectedFigters = [];
    this.fightersDetailsMap = new Map();
    this.createFighters(fighters);
  }

  fight(firstFighter: Fighter, seconFighter: Fighter): string {
    let winnerName = "";
    let count = 0;
    while (firstFighter.health && seconFighter.health) {
      if (count % 2) {
        let damage = firstFighter.getBlockPower() - seconFighter.getHitPower();
        if (damage < 0) {
          firstFighter.health += damage;
        }
      } else {
        let damage = seconFighter.getBlockPower() - firstFighter.getHitPower();
        if (damage < 0) {
          seconFighter.health += damage;
        }
      }
      count++;
    }
    if (firstFighter.health) {
      winnerName = firstFighter.name;
    } else {
      winnerName = seconFighter.name;
    }

    this.createWinnerPopup(winnerName);
    this.selectedFigters = [];

    return winnerName;
  }


  createFighters(fighters: Fighter[]) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleFighterClick, this.handleFighterInfoClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  createFightButton() {
    const fightButton = this.createElement({
      tagName: 'button',
      className: 'fight-btn',
    });

    fightButton.innerText = 'Fight';

    return fightButton;
  }

  createModal = () => {
    let modalBackground = this.createElement({ tagName: 'div', className: 'greyModal' }) as HTMLDivElement;
    let modal = this.createElement({ tagName: 'div', className: 'modalContent' }) as HTMLDivElement;

    let closeIcon = this.createElement({ tagName: 'div', className: 'modal-close-icon' }) as HTMLDivElement;
    closeIcon.innerHTML = '&times;';
    closeIcon.onclick = () => this.handleCloseClick(modal, modalBackground);

    modal.appendChild(closeIcon);

    return {
      modal,
      modalBackground
    }
  }

  createWinnerPopup(winnerName: string) {
    const { modal, modalBackground } = this.createModal();

    const winnerLabel = this.createElement({
      tagName: 'p',
      className: 'winner-label'
    });
    winnerLabel.innerText = `Winner is ${winnerName}`;

    modal.appendChild(winnerLabel);

    this.element.append(modalBackground);
    this.element.append(modal);
  }

  handleFighterInfoClick = async (event, fighter: Fighter) => {
    if(!this.fightersDetailsMap.has(fighter._id))
    {
      let fighterDetail = await fighterService.getFighterDetails(fighter._id) as Fighter;
      this.fightersDetailsMap.set(fighterDetail._id,fighterDetail);
    }

    var fighterDetail = this.fightersDetailsMap.get(fighter._id) as Fighter;

    const { modal, modalBackground } = this.createModal();

    // TODO: somehow pass name, health, attack, defense instead fighterDetail here
    if (fighterDetail !== undefined) {

      const form = this.createElement({
        tagName: 'form'
      });

      form.innerHTML = getDetailsForm(fighterDetail._id,fighterDetail.name,fighterDetail.health,fighterDetail.attack,fighterDetail.defense,fighterDetail);

      modal.appendChild(form);

    };

    this.element.append(modalBackground);
    this.element.append(modal);
  }

  handleFighterClick = (event, fighter: Fighter) => {
    this.selectedFigters = toggleFigterArray(fighter, this.selectedFigters, this.selectedFigters.length < 2);
 
    const selectedFigterElement = this.element.querySelector(`#fighter-${fighter._id}`);
    if (selectedFigterElement) {
      const isAlreadySelected = this.selectedFigters.find(selectedFigter => selectedFigter._id === fighter._id);
      selectedFigterElement.classList.toggle('selected', !!isAlreadySelected)
    }
 
    const fightButton = this.createFightButton() as HTMLButtonElement;
 
    if (this.selectedFigters.length === 2) {
      fightButton.onclick = () => {
        this.fight(this.selectedFigters[0], this.selectedFigters[1]);
        var allFighter=this.element.querySelectorAll('.fighter');
        allFighter.forEach(element => {
          element.classList.remove("selected");
        });
        this.element.removeChild(fightButton);
      }
      this.element.appendChild(fightButton);
    } else {
      const fightButton = this.element.querySelector(`.fight-btn`);
      if (fightButton) {
        this.element.removeChild(fightButton);
      }
    }
  }

  GetFighterValue(fighter: Fighter)
  {
    let name: string = (<HTMLInputElement>document.getElementById("fighterName")).value;
    (<HTMLInputElement>document.getElementById("fighterName")).value = "";
    let health: number = +(<HTMLInputElement>document.getElementById("fighterHealth")).value;
    (<HTMLInputElement>document.getElementById("fighterHealth")).value = "";
    let attack: number = +(<HTMLInputElement>document.getElementById("fighterAttack")).value;
    (<HTMLInputElement>document.getElementById("fighterAttack")).value = "";
    let defense: number = +(<HTMLInputElement>document.getElementById("fighterDefense")).value;
    (<HTMLInputElement>document.getElementById("fighterDefense")).value = "";
    
    if(name != "")
      fighter.name = name;
    if(health > 0)
      fighter.health = health;
    if(attack > 0)
      fighter.attack = attack;
    if(defense > 0)
      fighter.defense = defense;

  }

  handleCloseClick(modal: HTMLElement, modalBackground: HTMLElement) {
    this.element.removeChild(modal);
    this.element.removeChild(modalBackground);
  }
}

export default FightersView;