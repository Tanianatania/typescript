class Fighter {
  _id: number;
  name: string;
  health: number;
  attack: number;
  defense: number;
  source: string;

  constructor(id: number, _name: string, _health: number, _attack: number, _defense: number) {
    this._id = id;
    this.attack = _attack;
    this.defense = _defense;
    this.name = _name;
    this.health = _health;
    //this.source=_source;
  }


  getHitPower(): number {
    var criticalHitChance = Math.floor(Math.random() * Math.floor(2)) + 1;
    var power = this.attack * criticalHitChance;
    return power;
  }

  getBlockPower(): number {
    var dodgeChance = Math.floor(Math.random() * Math.floor(2)) + 1;
    var power = this.defense * dodgeChance;
    return power;
  }
}

export default Fighter;