class View {
  element: HTMLElement;

  createElement({ tagName, className = '', attributes = {}, id = '' }): HTMLElement {
    const element = document.createElement(tagName) as HTMLElement;
    if (className) {
      element.classList.add(className);
    }
    if (id) {
      element.id = id;
    }
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
  }
}

export default View;
