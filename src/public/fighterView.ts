import View from './view';
import Fighter from './fighter';

class FighterView extends View {
  constructor(fighter, handleClick, handleDetailsClick) {
    super();

    this.createFighter(fighter, handleClick, handleDetailsClick);
  }

  createFighter(fighter: Fighter, handleClick, handleDetailsClick) {
    const { name, source, attack, defense , health } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const detailsElement = this.createDetailsButton();

    this.element = this.createElement({ tagName: 'div', className: 'fighter', id: `fighter-${fighter._id}` });
    this.element.append(imageElement, nameElement, detailsElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
    detailsElement.addEventListener('click', event => handleDetailsClick(event, fighter), false);
  }

  createName(name: string) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source: string) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  createDetailsButton() {
    const buttonElement = this.createElement({
      tagName: 'button',
      className: 'details-btn',
    });

    buttonElement.innerText = 'Figter Details';

    return buttonElement;
  }
}

export default FighterView;